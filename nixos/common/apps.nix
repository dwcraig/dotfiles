{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    awscli2
    bat
    brave
    curl
    dbeaver
    discord
    element-desktop
    fd
    filezilla
    firefox
    fzf
    gcc
    gh
    gimp
    git
    gnumake
    go
    google-chrome
    htop
    inkscape
    inotify-tools
    insomnia
    jetbrains-mono
    jq
    kate
    keepassxc
    libreoffice
    microsoft-edge
    neofetch
    neovim
    nodejs
    php82
    php82Packages.composer
    ripgrep
    rustup
    spotify
    stow
    stripe-cli
    teams
    tmux
    vscode
    vlc
    wezterm
    wget
    zig
    zsh-autosuggestions
  ];

  services.flatpak.enable = true;
}
