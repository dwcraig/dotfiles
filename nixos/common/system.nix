{config, pkgs, ...}:

{
  services.fwupd.enable = true;

  virtualisation.docker.enable = true;

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };
}
