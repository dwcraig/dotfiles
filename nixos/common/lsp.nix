{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    rust-analyzer
    gopls
    nodePackages.intelephense
    nodePackages.typescript-language-server
    rnix-lsp
    zls
  ];
}
