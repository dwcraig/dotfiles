#!/bin/bash
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

cd ~/Downloads
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/CascadiaMono.zip
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.zip
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/CommitMono.zip
mkdir -p ~/.local/share/fonts
unzip JetBrainsMono.zip -d ~/.local/share/fonts/JetBrainsMono
unzip CascadiaMono.zip -d ~/.local/share/fonts/CascadiaMono
unzip CommitMono.zip -d ~/.local/share/fonts/CommitMono
fc-cache -f -v
rm -rf CascadiaMono.zip JetBrainsMono.zip CommitMono.zip
cd -
