#!/bin/bash

sudo apt install -y \
		autoconf \
		bison \
		build-essential \
		fop \
		gettext \
		libcurl4-openssl-dev \
		libedit-dev \
		libgd-dev \
		libgl1-mesa-dev \
		libglu1-mesa-dev \
		libicu-dev \
		libjpeg-dev \
		libmysqlclient-dev \
		libncurses-dev \
		libncurses5-dev \
		libonig-dev \
		libpng-dev \
		libpng-dev \
		libpq-dev \
		libreadline-dev \
		libsqlite3-dev \
		libssh-dev \
		libssl-dev \
		libwxgtk-webview3.0-gtk3-dev \
		libwxgtk3.0-gtk3-dev \
		libxml2-dev \
		libxml2-utils \
		libzip-dev \
		m4 \
		openjdk-11-jdk \
		openssl \
		pkg-config \
		re2c \
		unixodbc-dev \
		unzip \
		xsltproc \
		zlib1g-dev

git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.14.0

. "$HOME/.asdf/asdf.sh"

asdf plugin add nodejs
asdf install nodejs latest:20
asdf global nodejs latest:20

asdf plugin add golang
asdf install golang latest
asdf global golang latest

asdf plugin add erlang
asdf install erlang latest:26
asdf global erlang latest:26

asdf plugin add elixir
asdf install elixir 1.16.2-otp-26
asdf global elixir 1.16.2-otp-26

asdf plugin add rust
asdf install rust latest:stable
asdf global rust latest:stable

asdf plugin add php
asdf install php 8.3.4
asdf global php 8.3.4


