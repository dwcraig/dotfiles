alias c="code ."
alias n="nvim ."
alias sail="./vendor/bin/sail"
alias pa="php artisan"
alias gcm="git commit -m"
alias gs="git status"
alias gp="git push"
alias pn="pnpm"
alias l="ls -lah"
alias gcob="git checkout -b"
alias gpom="git push origin main"
alias gaa="git add -A"
alias fish_reload="source ~/.config/fish/config.fish"
alias cl="clear"

if not string match -q Darwin (uname) \
        && not string match -q nixos (hostname) \
        && not string match -q Microsoft (cat /proc/version | string collect)
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Alt>1']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Alt>2']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Alt>3']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Alt>4']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Alt>5']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Alt>6']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 "['<Alt>7']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 "['<Alt>8']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 "['<Alt>9']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-10 "['<Alt>0']"
end
