export PATH=/home/dwcraig/.local/bin:$PATH

if [[ -f "/opt/homebrew/bin/brew" ]] then
  # If you're using macOS, you'll want this enabled
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add in zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

# Add in snippets
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::archlinux
zinit snippet OMZP::aws
zinit snippet OMZP::kubectl
zinit snippet OMZP::kubectx
zinit snippet OMZP::command-not-found

# Load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/zen.toml)"

# Keybindings
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
bindkey '^[w' kill-region

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

alias b="brew"
alias bsl="brew services list"
alias bss="brew services start"
alias zshconfig="nvim ~/.zshrc"
alias nixconfig="sudo nvim /etc/nixos/configuration.nix"
alias nixupdate="sudo nixos-rebuild switch"
alias vim="nvim"
alias ts="tmux-sessionizer"
alias n="nvim ."
alias v="nvim ."
alias cat="bat"
alias sail="./vendor/bin/sail"
alias sdev="./vendor/bin/sail npm run dev"
alias sup="./vendor/bin/sail up"
alias sa="./vendor/bin/sail artisan"
alias a="php artisan"
alias pamfs="php artisan migrate:fresh --seed"
alias c="code ."
alias gcm="git commit -m"
alias gs="git status"
alias gp="git push"
alias gcob="git checkout -b"
alias pn="pnpm"
alias cl="clear"
alias lg="XDG_CONFIG_HOME=$HOME/.config lazygit"
alias r="./bin/rails"
alias ssh="TERM=xterm-256color ssh"
alias l="eza -lah"
alias cd="z"

eval "$(direnv hook zsh)"

# sst
export PATH=/home/dwcraig/.sst/bin:$PATH
export PATH=/Users/dwcraig/.sst/bin:$PATH
export PATH=$HOME/.local/scripts:$PATH

# Lando
export PATH="/Users/dwcraig/.lando/bin${PATH+:$PATH}"; #landopath

# check if fzf version is higher than 0.48.0
if [[ "$(fzf --version)" =~ "0.48.0" ]]; then
    eval "$(fzf --zsh)"
fi

# Zoxide
eval "$(zoxide init zsh)"

# Determine the path to mise based on the system
if [[ $(uname) == "Darwin" ]]; then
    mise_path="/opt/homebrew/bin/mise"
elif [[ $(uname) == "Linux" ]]; then
    mise_path="/usr/bin/mise"
else
    echo "Unsupported operating system."
    return 1
fi

# Check if mise exists at the determined path
if [[ -x "$mise_path" ]]; then
    eval "$($mise_path activate zsh)"
    eval "$($mise_path hook-env -s zsh)"
else
    echo "mise not found at $mise_path."
    return 1
fi
