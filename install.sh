# Install base dependencies
sudo pacman -Sy --needed git base-devel

mkdir -p $HOME/code/{p,work}
mkdir -p $HOME/.config/lazygit

COMMAND=yay
if command -v "$COMMAND" >/dev/null 2>&1; then
	echo "yay is installed. skipping installation."
else
	echo "yay not found. Installing..."
	# Install yay to manage AUR Applications
	git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin && makepkg -si
fi


sudo pacman -Sy \
	alacritty \
	bat \
	direnv \
	docker \
	eza \
	fd \
	fzf \
	github-cli \
	gnome-tweaks \
	inotify-tools \
	keepassxc \
	lazygit \
	ttf-jetbrains-mono-nerd \
	neovim \
	ripgrep \
	stow \
	tmux \
	zoxide \
	zsh

yay -Sy \
	1password \
	1password-cli \
	aws-cli-v2 \
	brave-bin \
	discord \
	oh-my-posh \
	mise \
	google-chrome

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
stow git nvim zsh lazygit scripts tmux
sudo usermod -aG docker $USER
